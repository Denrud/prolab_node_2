const os = require('os');

const model = os.cpus().map(item => item.model);

const getName = (el) => {
    return el[0];
}

module.exports = getName(model);