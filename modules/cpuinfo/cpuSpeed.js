const os = require('os');

const speed = os.cpus().map(item => item.speed);
const getSpeed = (el) => {
    return el[0];
}

module.exports = getSpeed(speed);