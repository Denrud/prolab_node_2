
const { cpuCores } = require('./cpuinfo/cpuCore.js');
const getName = require('./cpuinfo/cpuModel.js');
const getSpeed = require('./cpuinfo/cpuSpeed.js');

module.exports = {
    model: getName,
    speed: getSpeed,
    core: cpuCores
}